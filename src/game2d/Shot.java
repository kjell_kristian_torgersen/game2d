package game2d;

import java.util.List;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MoveAction;

public class Shot implements IMoveable
{
	long x;
	long y;

	I2D vel;

	int counter = 0;
	double animTimer;
	double timer;
	int state = 0;
	int base = 0xE0;
	List<IMoveable> other;

	public Shot(I2D pos, I2D vel, List<IMoveable> other)
	{
		this.x = pos.X() << 10;
		this.y = pos.Y() << 10;
		this.vel = vel;
		animTimer = 0;
		timer = 0;
		this.other = other;
	}

	@Override
	public int GetSprite()
	{

		return base + ((counter) & 0x3);
	}

	@Override
	public int X()
	{
		// TODO Auto-generated method stub
		return (int) (x >> 10);
	}

	@Override
	public int Y()
	{
		// TODO Auto-generated method stub
		return (int) (y >> 10);
	}

	@Override
	public boolean Iter(double dt)
	{
		x += vel.X() * (int) (1024.0 * dt);
		y += vel.Y() * (int) (1024.0 * dt);
		switch (state) {
		case 0:

			animTimer += dt;
			timer += dt;
			if (animTimer >= 0.125) {
				counter = (counter + 1) & 0x3;
				animTimer -= 0.125;
			}
			if (timer >= 3) {
				state = 1;
				counter = 0;
				base = 0xE4;
			}
			break;
		case 1:
			animTimer += dt;
			if (animTimer >= 0.125) {
				counter = (counter + 1) & 0x3;
				animTimer -= 0.125;
				if (counter == 0)
					return true;
			}
			break;
		}
		for (IMoveable movable : other) {
			if ((movable != this) && !(movable instanceof Player)) {
				int dx = this.X() - movable.X();
				int dy = this.Y() - movable.Y();
				double dist = (double) (dx * dx + dy * dy);
				//if (state != 1) {	
				//	state = 1;
					//counter = 0;
					//base = 0xE4;
					movable.Damage((int) (32.0 * 32.0 / dist));
				//}
				// }
			}
		}
		return false;
	}

	@Override
	public void SetVelocity(I2D velocity)
	{
		this.vel = velocity;

	}

	@Override
	public void Damage(int amount)
	{
		// TODO Auto-generated method stub

	}

}
