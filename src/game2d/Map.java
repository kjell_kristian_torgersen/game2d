package game2d;

import java.awt.Graphics;
import java.util.Random;

public class Map implements IMap
{
	private int[] map;

	private Const2D size;
	private Random rng;

	public Map(int sizex, int sizey, Random rng)
	{
		this.rng = rng;
		size = new Const2D(sizex, sizey);
		map = new int[size.X() * size.Y()];
		MakeMap();
	}

	/* (non-Javadoc)
	 * @see game2d.IMap#GetTile(int, int)
	 */
	@Override
	public int GetTile(int x, int y)
	{
		int x0 = x & (size.X() - 1);
		int y0 = y & (size.Y() - 1);
		return map[y0 * size.X() + x0];
	}

	private void MakeRoad(int direction, int length, int sx, int sy)
	{
		int rx = sx;
		int ry = sy;
		int tile = 0x2c;
		for (int i = 0; i < length; i++) {

			switch ((direction) & 0x3) {
			case 0:
				tile = 0x2D;
				rx++;
				break;
			case 1:
				tile = 0x3C;
				ry--;
				break;
			case 2:
				tile = 0x2D;
				rx--;
				break;
			case 3:
				tile = 0x3C;
				ry++;
				break;
			default:
				break;
			}
			if (rx < 0)
				rx += size.X();
			if (ry < 0)
				ry += size.Y();
			if (rx >= size.X())
				rx -= size.X();
			if (ry >= size.Y())
				ry -= size.Y();
			if (map[ry * size.X() + rx] == -1) {
				map[ry * size.X() + rx] = tile;
			} else {
				map[ry * size.X() + rx] = 0x3D;
			}
			int[] nexdiru = new int[] { 0x4E, 0x2E, 0x2C, 0x4C };
			if (rng.nextInt(100) == 0) {
				// if(rng.nextBoolean()) {
				if (map[ry * size.X() + rx] != -1) {
					map[ry * size.X() + rx] = nexdiru[direction & 0x3];
					direction++;

				} else {
					map[ry * size.X() + rx] = 0x3D;
					direction++;
				}

				// } else { direction--; }

			}
			if (rng.nextInt(50) == 0) {
				MakeRoad((direction + 1) & 0x3, length / 2, rx, ry);
			}
		}
	}

	private void MakeMap()
	{
		for (int i = 0; i < map.length; i++) {
			if (i < size.X()) {
				map[i] = 0x48 + rng.nextInt(3);// rng.nextInt()
			} else {
				map[i] = -1;
				if (rng.nextDouble() > 0.9)
					map[i] = 0x67 + rng.nextInt(5);
			}
		}
		// MakeRoad(3, 400, 10, 10);

	}

	public void SetView(int vx, int vy)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public I2D GetSize()
	{
		// TODO Auto-generated method stub
		return size;
	}

}
