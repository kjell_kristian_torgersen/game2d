package game2d;

public interface IMoveable extends IEntity
{
	public boolean Iter(double dt);

	public void SetVelocity(I2D velocity);

	public void Damage(int amount);
}
