package game2d;

public interface IMap
{

	public abstract int GetTile(int x, int y);
	public abstract I2D GetSize();
}