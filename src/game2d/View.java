package game2d;

import java.awt.Button;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

public class View extends JPanel implements KeyListener, Observer, MouseListener
{
	private static final long serialVersionUID = -9055749045238070013L;
	private Game game;
	private String info;
	private int vx = 0;
	private int vy = 0;

	public View(Game game)
	{
		this.game = game;
		addKeyListener(this);
		addMouseListener(this);
		setFocusable(true);
	}

	public void DrawSprite(Graphics g, int x, int y, int sprite)
	{
		int sx = sprite & 0xF;
		int sy = (sprite >> 4) & 0xF;
		g.drawImage(game.GetSprites(), x - 16, y - 16, x + 32 - 16, y + 32 - 16, 32 * sx, 32 * sy, 32 * (sx + 1), 32 * (sy + 1), this);
	}

	@Override
	public void paint(Graphics g)
	{
		// BufferedImage Sprites = game.GetSprites();
		int tile = 0x2c;
		I2D center = game.GetCenter();

		g.clearRect(0, 0, getWidth(), getHeight());
		for (int y = 0; y < 2 + getHeight() / 32; y++) {
			for (int x = 0; x < 2 + getWidth() / 32; x++) {
				tile = game.GetMap().GetTile(x + center.X() / 32, y + center.Y() / 32);
				if (tile >= 0) {
					DrawSprite(g, 32 * x - (center.X() & 31), 32 * y - (center.Y() & 31), tile);
				}
			}
		}
		for (IEntity ent : game.GetIMoveables()) {
			DrawSprite(g, ent.X() - center.X() + getWidth() / 2, ent.Y() - center.Y() + getHeight() / 2, ent.GetSprite());
		}
		g.setColor(Color.black);
		g.drawString(info, 10, getWidth() - 10);

	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		// TODO Auto-generated method stub
		System.out.println(e);

	}

	@Override
	public void keyPressed(KeyEvent e)
	{
		// TODO Auto-generated method stub
		System.out.println(e);
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			vy = -48;
			break;
		case KeyEvent.VK_DOWN:
			vy = 48;
			break;
		case KeyEvent.VK_LEFT:
			vx = -48;
			break;
		case KeyEvent.VK_RIGHT:
			vx = 48;
			break;
		case KeyEvent.VK_ESCAPE:
			System.exit(0);
			break;
		}
		game.Move(vx, vy);
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		System.out.println(e);
		// TODO Auto-generated method stub
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			vy = 0;
			break;
		case KeyEvent.VK_DOWN:
			vy = 0;
			break;
		case KeyEvent.VK_LEFT:
			vx = 0;
			break;
		case KeyEvent.VK_RIGHT:
			vx = 0;
			break;
		case KeyEvent.VK_SPACE:
			game.Spawn(new Shot(game.GetCenter(), new Const2D(5 * vx, 5 * vy), game.GetIMoveables()));
			break;
		case KeyEvent.VK_ESCAPE:
			System.exit(0);
			break;
		}
		game.Move(vx, vy);
	}

	@Override
	public void update(Observable o, Object arg)
	{
		info = (String) arg;
		repaint();

	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		int cx = getWidth() / 2;
		int cy = getHeight() / 2;

		if (e.getButton() == MouseEvent.BUTTON1) {
			game.Spawn(new Shot(game.GetCenter(), new Const2D(e.getX() - cx, e.getY() - cy), game.GetIMoveables()));
		} else if (e.getButton() == MouseEvent.BUTTON3) {
			int x = game.GetCenter().X() + e.getX() - cx;
			int y = game.GetCenter().Y() + e.getY() - cy;
			game.Spawn(new Bomb(x, y, game.GetIMoveables()));
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		// TODO Auto-generated method stub

	}

}
