package game2d;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class SpriteBatch
{
	private BufferedImage Sprites = null;

	public SpriteBatch()
	{

	}

	public SpriteBatch(String filename)
	{
		// 
		this.Sprites = ReadImage(filename);
	}

	private BufferedImage ReadImage(String filename)
	{
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(filename));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return img;

	}

	public BufferedImage GetSprites()
	{
		return Sprites;
	}
}
