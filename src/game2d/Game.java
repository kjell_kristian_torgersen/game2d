package game2d;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Random;

import javax.swing.Timer;

public class Game extends Observable
{
	private IMap map = null;
	private Random rng = null;
	private SpriteBatch spriteBatch = null;

	private List<IMoveable> moveables;
	private TimerClass timerClass;
	Player player;

	public I2D GetCenter()
	{
		return player;
	}

	public Game()
	{
		player = new Player();

		rng = new Random();
		map = new ConstMap();
		spriteBatch = new SpriteBatch("/home/kjell/git/spriteedit/SpriteEdit/tiles.png");
		moveables = new ArrayList<IMoveable>();
		moveables.add(player);
		timerClass = new TimerClass(20);

	}

	public void Spawn(IMoveable movable)
	{
		moveables.add(movable);
	}

	public List<IMoveable> GetIMoveables()
	{
		return moveables;
	}

	public void SetSpriteBatch(SpriteBatch spriteBatch)
	{
		this.spriteBatch = spriteBatch;
	}

	public BufferedImage GetSprites()
	{
		return spriteBatch.GetSprites();
	}

	public void SetRng(Random rng)
	{
		this.rng = rng;
	}

	public void SetMap(IMap map)
	{
		this.map = map;
	}

	public IMap GetMap()
	{
		return map;
	}

	public void Move(int dx, int dy)
	{
		player.SetVelocity(new Const2D(dx, dy));
	}

	class TimerClass implements ActionListener
	{
		private Timer timer;
		long lasttimemillis;

		public TimerClass(int interval)
		{
			lasttimemillis = System.currentTimeMillis();
			timer = new Timer(interval, this);
			timer.setRepeats(true);
			timer.start();
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			long currenttimemillis = System.currentTimeMillis();
			double dt = (currenttimemillis - lasttimemillis) / 1000.0;
			lasttimemillis = currenttimemillis;

			Iterator<IMoveable> it = moveables.iterator();
			while (it.hasNext()) {
				IMoveable mov = it.next();
				if (mov.Iter(dt))
					it.remove();
			}
			//System.out.println(1.0 / dt);
			setChanged();
			notifyObservers("Fps = " + 1.0 / dt);

		}
	}
}
