package game2d;

public class Pos2D implements IPos2D
{

	private final int x;
	private final int y;
	
	public Pos2D(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	@Override
	public int X()
	{
		return x;
	}

	@Override
	public int Y()
	{
		return y;
	}

}
