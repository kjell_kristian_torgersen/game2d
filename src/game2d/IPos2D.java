package game2d;

public interface IPos2D
{
	public int X();
	public int Y();
}
