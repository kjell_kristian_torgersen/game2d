package game2d;

public class Player implements IMoveable
{
	private I2D vel;
	
	long x;
	long y;
	
	public Player()
	{
		vel = new V2D(0, 0);
	}

	@Override
	public int X()
	{
		// TODO Auto-generated method stub
		return (int)(x >> 10);
	}

	@Override
	public int Y()
	{
		// TODO Auto-generated method stub
		return (int)(y >> 10);
	}

	@Override
	public int GetSprite()
	{
		// TODO Auto-generated method stub
		return 0x65;
	}

	@Override
	public void SetVelocity(I2D newVel)
	{
		vel = newVel;
	}

	@Override
	public boolean Iter(double dt)
	{
		x += vel.X()*(int)(1024.0*dt);
		y += vel.Y()*(int)(1024.0*dt);
		return false;
	}

	@Override
	public void Damage(int amount)
	{
		// TODO Auto-generated method stub
		
	}

}
