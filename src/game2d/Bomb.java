package game2d;

import java.util.List;

public class Bomb implements IMoveable
{
	double timer = 0;
	int state = -1;
	long x;
	long y;
	private I2D vel;
	int health;
	List<IMoveable> other;

	public Bomb(I2D pos, List<IMoveable> other)
	{
		x = pos.X() << 10;
		y = pos.Y() << 10;
		this.vel = new Const2D(0, 0);
		this.health = 10;
		this.other = other;
	}

	public Bomb(int x, int y, List<IMoveable> other)
	{
		this.x = x << 10;
		this.y = y << 10;
		this.vel = new Const2D(0, 0);
		this.health = 10;
		this.other = other;
	}

	@Override
	public int GetSprite()
	{
		// TODO Auto-generated method stub
		if(state == -1) return 0xF0;
		return 0xF0 + state;
	}

	@Override
	public int X()
	{
		// TODO Auto-generated method stub
		return (int) (x >> 10);
	}

	@Override
	public int Y()
	{
		// TODO Auto-generated method stub
		return (int) (y >> 10);
	}

	@Override
	public boolean Iter(double dt)
	{
		timer += dt;
		x += vel.X() * (int) (1024.0 * dt);
		y += vel.Y() * (int) (1024.0 * dt);
		
		if ((health <= 0) && (state < 12)) {
			state = 12;
		}


		if ((state >= 0) && (state < 12)) {
			if (timer >= 0.5) {
				timer -= 0.5;
				state = (state + 1) & 15;
			}
		} else if(state >= 12){
			if (state == 13) {
				for (IMoveable movable : other) {
					if ((movable != this) && !(movable instanceof Player)) {
						int dx = this.X() - movable.X();
						int dy = this.Y() - movable.Y();
						double dist = (double) (dx * dx + dy * dy);
						movable.Damage((int) (10.0 * 1024.0 / dist));
					}
				}
			}

			if (timer >= 0.100) {
				timer -= 0.100;
				state = (state + 1) & 15;
				if (state == 0)
					return true;
			}
		} else {
			timer = 0;
		}
		

		return false;
		// TODO Auto-generated method stub

	}

	@Override
	public void SetVelocity(I2D velocity)
	{
		this.vel = velocity;
	}

	@Override
	public void Damage(int amount)
	{
		this.health -= amount;
		if(state == -1 && amount > 0) state = 0;
		System.out.println("amount = " + amount);
		System.out.println("Bomb.health = " + this.health);

	}

}
