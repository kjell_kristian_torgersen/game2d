package game2d;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;


public class Window extends JFrame
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4759805052863667732L;
	
	private View view;
	
	public Window(Game game)
	{
		super();
		// _window = new JFrame();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		view = new View(game);
		game.addObserver(view);
		//setJMenuBar(CreateMenuBar());

		getContentPane().add(view);
		pack();
		setSize(640, 480);
		setVisible(true);
		

	}
}
