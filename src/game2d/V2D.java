package game2d;

public class V2D implements I2D
{
	private int _x;
	private int _y;

	public V2D(I2D kopierFra)
	{
		this._x = kopierFra.X();
		this._y = kopierFra.Y();
		//Init(kopierFra._x, kopierFra._y);
	}

	// Lager 0 vektor
	public V2D()
	{
		this._x = 0;
		this._y = 0;
	}

	// lager vektor for x,y - koordinater
	public V2D(int x, int y)
	{
		this._x = x;
		this._y = y;
	}

	public void PlussLik(I2D annen) 
	{
		this._x += annen.X();
		this._y += annen.Y();
	}
	
	public void MinusLik(I2D annen) 
	{
		this._x -= annen.X();
		this._y -= annen.Y();
	}
	
	public void GangerLik(I2D annen) 
	{
		this._x *= annen.X();
		this._y *= annen.Y();
	}
	
	public void DeltpaaLik(I2D annen) 
	{
		this._x /= annen.X();
		this._y /= annen.Y();
	}
	
	public void GangerLik(int annen) 
	{
		this._x *= annen;
		this._y *= annen;
	}
	
	public void DeltpaaLik(int annen) 
	{
		this._x /= annen;
		this._y /= annen;
	}
	
	// Ny vektor + annen vektor
	public V2D Pluss(I2D annen)
	{
		return new V2D(this._x + annen.X(), this._y + annen.Y());
	}

	public V2D Minus(I2D annen)
	{
		return new V2D(this._x - annen.X(), this._y - annen.Y());
	}

	public V2D Pluss(int annen)
	{
		return new V2D(this._x + annen, this._y + annen);
	}

	public V2D Minus(int annen)
	{
		return new V2D(this._x - annen, this._y - annen);
	}

	public V2D Ganger(int k)
	{
		return new V2D(k * this._x, k * this._y);
	}

	public V2D Deltpaa(int d)
	{
		return new V2D(this._x / d, this._y / d);
	}

	public int X()
	{
		return _x;
	}

	public int Y()
	{
		return _y;
	}
	
	public void SetX(int x)
	{
		 _x = x;
	}

	public void SetY(int y)
	{
		_y = y;
	}

	public String toString()
	{
		return "(" + _x + ";" + _y + ")";
	}

	public void PlussLik(int dx, int dy)
	{
		this._x += dx;
		this._y += dy;
		
	}
}
